# Commented out IPython magic to ensure Python compatibility.
# %tensorflow_version 2.x
import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras import layers

# DONE
# acgan, smaller arch
# upsampling2d, dropout
# multiple instead of concat (canceled)
# google colab
# smooth labels
# noisy labels
# embedding layer, concat to an activation(dense()) layer before conv
# reverted from embedding layer
# Saving weights
# Logging to tboard https://medium.com/@tommytao_54597/use-tensorboard-in-google-colab-16b4bb9812a6 and https://stackoverflow.com/questions/44861149/keras-use-tensorboard-with-train-on-batch

# TODO
# Smaller kernels helpful?
# Plots https://github.com/lukedeo/keras-acgan/blob/master/acgan-analysis.ipynb

# ONTO MOONBOARD
# Visualizing holds on board
# B/W analogy
# Testing various archs

DEFAULT_CONFIG = {
    'name': 'acgan-mnist-bw-tboard',
    'seed': 1337,
    # Training config
    'train_epochs': int(1e4),
    'train_minibatch_size': 64,
    'train_save_each': 200,
    'train_labels_α': 0.9,
    'train_labels_noise': 0.05,
    # Model hyperparams
    'rmsprop_lr': 2e-4,
    'rmsprop_decay': 6e-8,
    'n_classes': 10,
    'latent_dim': 100,
    'lrelu_α': 0.2,
    'dropout_p': 0.25,
    'batchnorm_momentum': 0.8,
    # Model architectures
    'G_arch': {
        'init_shape': (7, 7, 128),
        'filters': [128, 64, 1],
        'kernels': [5, 5, 5],
        'strides': [1, 1, 1],
        'upsamples': [2, 2, 0],
        'out_activation': 'sigmoid'
    },
    'D_arch': {
        'data_shape': (28, 28, 1),
        'filters': [16, 32, 64, 128],
        'kernels': [3, 3, 3, 3],
        'strides': [2, 2, 2, 1]
    }
}

class GAN:
    def __init__(self, config, data_fn, infile=None):
        # Config stuff
        self.config = config
        if config['seed']:
          tf.random.set_seed(config['seed'])
          np.random.seed(config['seed'])
        os.makedirs(config['name'], exist_ok=True)
        os.chdir(config['name'])
        os.makedirs('images', exist_ok=True)
        os.makedirs('models', exist_ok=True)
        # Create (or load) models
        if not infile:
            self.create_models()
        else:
            raise NotImplementedError('Loading from trained models not implemented')
        # Load data
        self.target_data, _ = data_fn()
        self.classes = np.identity(self.config['n_classes'])      

    def generator_architecture(self):
        # Config variables
        α = self.config['lrelu_α']
        m = self.config['batchnorm_momentum']
        init_shape = self.config['G_arch']['init_shape']
        upsamples, filters, kernels, strides = self.config['G_arch']['upsamples'], self.config['G_arch']['filters'], self.config['G_arch']['kernels'], self.config['G_arch']['strides']
        out_activation = self.config['G_arch']['out_activation']
        # Input layers
        in_z = layers.Input(shape=self.config['latent_dim'])
        in_c = layers.Input(shape=self.config['n_classes'])
        # Embedding for c
        y = layers.Dense(self.config['latent_dim'])(in_c)
        y = layers.ReLU()(y)
        # Scale to initial 'image' size
        gen = layers.Concatenate()([in_z, y])  # TODO consider architecture as in https://github.com/keras-team/keras/blob/master/examples/mnist_acgan.py
        gen = layers.Dense(init_shape[0] * init_shape[1] * init_shape[2])(gen)
        gen = layers.ReLU()(gen)
        gen = layers.Reshape(init_shape)(gen)
        # The good stuff
        for u, f, k, s in zip(upsamples, filters, kernels, strides):
            if u > 0: gen = layers.UpSampling2D(size=u)(gen)
            gen = layers.BatchNormalization(momentum=m)(gen)
            gen = layers.ReLU()(gen)
            gen = layers.Conv2D(filters=f, kernel_size=k, strides=s, padding='same')(gen)
        # Output
        out = layers.Activation(out_activation, name='G_Out')(gen)
        return tf.keras.Model([in_z, in_c], out, name='Generator')  

    def discriminator_architecture(self):
        # Config variables
        α = self.config['lrelu_α']
        p = self.config['dropout_p']
        m = self.config['batchnorm_momentum']
        data_shape = self.config['D_arch']['data_shape']
        filters, kernels, strides = self.config['D_arch']['filters'], self.config['D_arch']['kernels'], self.config['D_arch']['strides']
        # Input layer
        in_x = layers.Input(shape=data_shape)
        # The good stuff
        features = in_x
        for f, k, s in zip(filters, kernels, strides):
            if p > 0: features = layers.Dropout(p)(features)
            features = layers.LeakyReLU(α)(features)
            features = layers.Conv2D(filters=f, kernel_size=k, strides=s, padding='same')(features)
        # Output
        features = layers.Flatten()(features)
        out_prob = layers.Dense(1, activation='sigmoid', name='D_out_prob')(features)
        out_classes = layers.Dense(self.config['n_classes'], activation='softmax', name='D_out_classes')(features)
        return tf.keras.Model(in_x, [out_prob, out_classes], name='Discriminator')

    def create_models(self):
        lr, decay, loss = self.config['rmsprop_lr'], self.config['rmsprop_decay'], ['binary_crossentropy', 'categorical_crossentropy']
        # Discriminator
        self.D = self.discriminator_architecture()
        self.D.compile(loss=loss, optimizer=tf.keras.optimizers.RMSprop(lr=lr, decay=decay))
        # Generator (no compile as we don't train directly on it)
        self.G = self.generator_architecture()
        # Combined model
        self.D.trainable = False
        gan_in = self.G.input
        gan_out = self.D(self.G.output)
        self.combined_model = tf.keras.Model(gan_in, gan_out, name='Combined')
        self.combined_model.compile(loss=loss, optimizer=tf.keras.optimizers.RMSprop(lr=lr/2, decay=decay/2))
        # Plot model architectures to file
        plot_model = lambda m: tf.keras.utils.plot_model(m, to_file='images/%s.png' % m.name, show_shapes=True, expand_nested=True)
        plot_model(self.D)
        plot_model(self.G)
        plot_model(self.combined_model)

    def train(self):
        # Training vars
        epochs = self.config['train_epochs']
        m = self.config['train_minibatch_size']
        save_each = self.config['train_save_each']
        truelabels_real = np.repeat(self.config['train_labels_α'], m)
        truelabels_fake = np.repeat(0.0, m)
        truelabels_combined = np.concatenate([truelabels_fake, truelabels_real])

        # Set up tensorboard
        tensorboard_d = tf.keras.callbacks.TensorBoard(log_dir='../logs/%s' % self.config['name'], histogram_freq=0, write_graph=False, write_images=False)
        tensorboard_d.set_model(self.D)
        tensorboard_g = tf.keras.callbacks.TensorBoard(log_dir='../logs/%s' % self.config['name'], histogram_freq=0, write_graph=False, write_images=False)
        tensorboard_g.set_model(self.G)

        for epoch in range(epochs):
            # Sample from m samples from the generator and target distribution each
            fake_c = self.sample_classes(m)
            fake_x = self.sample_gen(self.sample_noise(m), fake_c)
            target_x, target_c = self.sample_target(m)
            # Update D
            samples_x = np.concatenate([fake_x, target_x])
            samples_c = np.concatenate([fake_c, target_c])
            loss_d = self.D.train_on_batch(samples_x, [self.flip_labels(truelabels_combined), samples_c])
            # Sample m samples for generator input
            noise, classes = self.sample_noise(m), self.sample_classes(m)
            # Update G
            loss_g = self.combined_model.train_on_batch([noise, classes], [truelabels_real, classes])

            # Log to Tensorboard
            self.log_tboard(tensorboard_d, tensorboard_g, loss_d, loss_g, epoch)

            # Save plots and models
            if epoch == 0 or not ((epoch+1) % save_each):
                self.save_plots(epoch)
                self.save_models(epoch)

    # Flips labels (real-to-fake, fake-to-real) with a given probability
    def flip_labels(self, labels):
      prob, real, fake = self.config['train_labels_noise'], self.config['train_labels_α'], 0
      flip = lambda x: x if np.random.uniform() >= prob else (real - x)
      return np.array(flip(labels))

    # Returns n random classes (not as one-hot vectors)
    def sample_classes(self, n):
        indices = np.random.randint(low=0, high=10, size=n)
        return self.classes[indices]
        # return indices
    
    # Returns n samples from the latent prior (each sample is a vector)
    def sample_noise(self, n):
        return np.random.normal(0, 1, (n, self.config['latent_dim']))
    
    # Returns samples from the generator model, given some noise and conditioned on class labels
    def sample_gen(self, noise, classes):
        return self.G.predict([noise, classes])

    # Returns n samples from the true data distribution
    def sample_target(self, n):
        indices = np.random.randint(low=0, high=self.target_data['x'].shape[0], size=n)
        samples = self.target_data['x'][indices]
        classes = self.classes[self.target_data['classes'][indices]]
        # classes = self.target_data['classes'][indices]
        return samples, classes

    def save_plots(self, epoch):
        r, c = 10, 10
        classes = np.array([num for _ in range(r) for num in range(c)])
        examples = self.sample_gen(self.sample_noise(r * c), self.classes[classes])
        # examples = self.sample_gen(self.sample_noise(r * c), classes)
        for i in range(r * c):
            plt.subplot(r, c, 1 + i)
            plt.axis('off')
            plt.imshow(examples[i, :, :, 0], cmap='gray')
        plt.savefig("images/%d.png" % epoch)
        plt.close()
    
    # Saves G and D to file (full model if first save, else just weights)
    def save_models(self, epoch):
      # if full:
      self.G.save('models/gen%d.h5' % epoch)
      self.D.save('models/disc%d.h5' % epoch)
      # else:
      #   self.G.save_weights('models/gen%d' % epoch)
      #   self.D.save_weights('models/disc%d' % epoch)
    
    # https://stackoverflow.com/questions/44861149/keras-use-tensorboard-with-train-on-batch
    def named_logs(self, model, logs):
      result = {}
      for name, val in zip(model.metrics_names, logs):
        result[name] = val
      return result

    def log_tboard(self, tb_d, tb_g, logs_d, logs_g, epoch):
      print('→%d\t [ D loss=%.3f, loss_p=%.3f, loss_c=%.3f ],\t [ G loss=%.3f, loss_p=%.3f, loss_c=%.3f ]' % (epoch, *logs_d, *logs_g))
      tb_d.on_epoch_end(epoch, {
          'Discriminator_Ls': logs_d[1],
          'Discriminator_Lc': logs_d[2]
      })
      tb_d.on_epoch_end(epoch, {
          'Generator_Ls': logs_g[1],
          'Generator_Lc': logs_g[2]
      })

def bwfilter(img, thresh=.25):
    img2 = img.copy()
    mask = img2 > thresh
    img2[mask] = 1
    img2[np.invert(mask)] = 0
    return img2

def data_fn():
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    x_train = x_train.astype(np.float32) / 255
    x_test = x_test.astype(np.float32) / 255
    x_train = x_train.reshape(*x_train.shape, 1)
    x_test = x_test.reshape(*x_test.shape, 1)
    for i in range(x_test.shape[0]):
      x_test[i] = bwfilter(x_test[i])
    for i in range(x_train.shape[0]):
      x_train[i] = bwfilter(x_train[i])
    return ({ 'x': x_train, 'classes': y_train }), ({ 'x': x_test, 'classes': y_test })

# Commented out IPython magic to ensure Python compatibility.
# Mount GDrive
# from google.colab import drive
# drive.mount('/content/drive')
# os.chdir('/content/drive/My Drive/colab')
# os.makedirs('logs', exist_ok=True)

# Setup Tensorboard thru Google Colab
# !wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip
# !unzip ngrok-stable-linux-amd64.zip
# get_ipython().system_raw('tensorboard --logdir /content/drive/My\ Drive/colab/logs --host 0.0.0.0 --port 6006 &')
# get_ipython().system_raw('./ngrok http 6006 &')
# ! curl -s http://localhost:4040/api/tunnels

# import tensorboard
# %load_ext tensorboard
# %tensorboard --logdir /content/drive/My\ Drive/colab/logs
# tensorboard.notebook.display(port=6006, height=1000)

# os.chdir('/content/drive/My Drive/colab')
# os.makedirs('logs', exist_ok=True)

config = DEFAULT_CONFIG
gan = GAN(config, data_fn)
gan.train()

drive.flush_and_unmount()
import tensorflow as tf
import numpy as np

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import fashion_mnist

from matplotlib import pyplot


def bwfilter(img, thresh=35):
    img2 = img.copy()
    mask = img2 > thresh
    img2[mask] = 255
    img2[np.invert(mask)] = 0
    return img2


# define the standalone discriminator model
# 2x2 stride for downsampling (Conv2D): https://machinelearningmastery.com/padding-and-stride-for-convolutional-neural-networks/
# Adam optim: https://machinelearningmastery.com/adam-optimization-algorithm-for-deep-learning/
def define_discriminator(in_shape=(28,28,1), n_classes=10):
	# label input
	in_label = layers.Input(shape=(1,))
	# embedding for categorical input
	li = layers.Embedding(n_classes, 50)(in_label)
	# scale up to image dimensions with linear activation
	n_nodes = in_shape[0] * in_shape[1]
	li = layers.Dense(n_nodes)(li)
	# reshape to additional channel
	li = layers.Reshape((in_shape[0], in_shape[1], 1))(li)
	# image input
	in_image = layers.Input(shape=in_shape)
	# concat label as a channel
	merge = layers.Concatenate()([in_image, li])
	# downsample
	fe = layers.Conv2D(128, (3,3), strides=(2,2), padding='same')(merge)
	fe = layers.LeakyReLU(alpha=0.2)(fe)
	# downsample
	fe = layers.Conv2D(128, (3,3), strides=(2,2), padding='same')(fe)
	fe = layers.LeakyReLU(alpha=0.2)(fe)
	# flatten feature maps
	fe = layers.Flatten()(fe)
	# dropout
	fe = layers.Dropout(0.4)(fe)
	# output
	out_layer = layers.Dense(1, activation='sigmoid')(fe)
	# define model
	model = tf.keras.Model([in_image, in_label], out_layer)
	# compile model
	opt = tf.keras.optimizers.Adam(lr=0.0002, beta_1=0.5)
	model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
	return model


# define the standalone generator model
# kernel is twice the stride
# leakyrelu and tanh
def define_generator(latent_dim, n_classes=10):
	# label input
	in_label = layers.Input(shape=(1,))
	# embedding for categorical input
	li = layers.Embedding(n_classes, 50)(in_label)
	# linear multiplication
	n_nodes = 7 * 7
	li = layers.Dense(n_nodes)(li)
	# reshape to additional channel
	li = layers.Reshape((7, 7, 1))(li)
	# image generator input
	in_lat = layers.Input(shape=(latent_dim,))
	# foundation for 7x7 image
	n_nodes = 128 * 7 * 7
	gen = layers.Dense(n_nodes)(in_lat)
	gen = layers.LeakyReLU(alpha=0.2)(gen)
	gen = layers.Reshape((7, 7, 128))(gen)
	# merge image gen and label input
	merge = layers.Concatenate()([gen, li])
	# upsample to 14x14
	gen = layers.Conv2DTranspose(128, (4,4), strides=(2,2), padding='same')(merge)
	gen = layers.LeakyReLU(alpha=0.2)(gen)
	# upsample to 28x28
	gen = layers.Conv2DTranspose(128, (4,4), strides=(2,2), padding='same')(gen)
	gen = layers.LeakyReLU(alpha=0.2)(gen)
	# output
	out_layer = layers.Conv2D(1, (7,7), activation='tanh', padding='same')(gen)
	# define model
	model = tf.keras.Model([in_lat, in_label], out_layer)
	return model


# define the combined generator and discriminator model, for updating the generator
# note that we define discriminator as untrainable - just for this model
def define_gan(g_model, d_model):
	# make weights in the discriminator not trainable
	d_model.trainable = False
	# get noise and label inputs from generator model
	gen_noise, gen_label = g_model.input
	# get image output from the generator model
	gen_output = g_model.output
	# connect image output and label input from generator as inputs to discriminator
	gan_output = d_model([gen_output, gen_label])
	# define gan model as taking noise and label and outputting a classification
	model = tf.keras.Model([gen_noise, gen_label], gan_output)
	# compile model
	opt = tf.keras.optimizers.Adam(lr=0.0002, beta_1=0.5)
	model.compile(loss='binary_crossentropy', optimizer=opt)
	return model


# load fashion mnist images
def load_real_samples():
	# load dataset
	(trainX, trainy), (_, _) = fashion_mnist.load_data()
	# expand to 3d, e.g. add channels
	X = np.expand_dims(trainX, axis=-1)
	# convert from ints to floats
	X = X.astype('float32')
	# scale from [0,255] to [-1,1]
	X = (X - 127.5) / 127.5
	return [X, trainy]
 

# select real samples
def generate_real_samples(dataset, n_samples):
	# split into images and labels
	images, labels = dataset
	# choose random instances
	ix = np.random.randint(0, images.shape[0], n_samples)
	# select images and labels
	X, labels = images[ix], labels[ix]
	# generate class labels
	y = np.ones((n_samples, 1))
	return [X, labels], y


# generate points in latent space as input for the generator
def generate_latent_points(latent_dim, n_samples, n_classes=10):
	# generate points in the latent space
	x_input = np.random.randn(latent_dim * n_samples)
	# reshape into a batch of inputs for the network
	z_input = x_input.reshape(n_samples, latent_dim)
	# generate labels
	labels = np.random.randint(0, n_classes, n_samples)
	return [z_input, labels]
 

# use the generator to generate n fake examples, with class labels
def generate_fake_samples(generator, latent_dim, n_samples):
	# generate points in latent space
	z_input, labels_input = generate_latent_points(latent_dim, n_samples)
	# predict outputs
	images = generator.predict([z_input, labels_input])
	# create class labels
	y = np.zeros((n_samples, 1))
	return [images, labels_input], y


# train the generator and discriminator
def train(g_model, d_model, gan_model, dataset, latent_dim, n_epochs=100, n_batch=128):
	bat_per_epo = int(dataset[0].shape[0] / n_batch)
	half_batch = int(n_batch / 2)
	# manually enumerate epochs
	for i in range(n_epochs):
		# enumerate batches over the training set
		for j in range(bat_per_epo):
			# get randomly selected 'real' samples
			[X_real, labels_real], y_real = generate_real_samples(dataset, half_batch)
			# update discriminator model weights
			d_loss1, _ = d_model.train_on_batch([X_real, labels_real], y_real)
			# generate 'fake' examples
			[X_fake, labels], y_fake = generate_fake_samples(g_model, latent_dim, half_batch)
			# update discriminator model weights
			d_loss2, _ = d_model.train_on_batch([X_fake, labels], y_fake)
			# prepare points in latent space as input for the generator
			[z_input, labels_input] = generate_latent_points(latent_dim, n_batch)
			# create inverted labels for the fake samples
			y_gan = np.ones((n_batch, 1))
			# update the generator via the discriminator's error
			g_loss = gan_model.train_on_batch([z_input, labels_input], y_gan)
			# summarize loss on this batch
			print('>%d, %d/%d, d1=%.3f, d2=%.3f g=%.3f' %
				(i+1, j+1, bat_per_epo, d_loss1, d_loss2, g_loss))
		# plot examples from this epoch
		save_plot(g_model, 10, i+1)
		# save the generator model from this epoch
		g_model.save('generator%d.h5' % i+1)


# create and save a plot of generated images (reversed grayscale)
def save_plot(model, n, epoch):
	# generate images
	latent_points = generate_latent_points(100, n * n)
	examples = model.predict(latent_points)
	# plot images
	for i in range(n * n):
		# define subplot
		pyplot.subplot(n, n, 1 + i)
		# turn off axis
		pyplot.axis('off')
		# plot raw pixel data
		pyplot.imshow(examples[i, :, :, 0], cmap='gray_r')
	pyplot.savefig("images/%d.png" % epoch)
	pyplot.close()


if __name__ == '__main__':
	# FOR TRAINING
	# size of the latent space
	latent_dim = 100
	# create the discriminator
	discriminator = define_discriminator()
	# create the generator
	generator = define_generator(latent_dim)
	# create the gan
	gan_model = define_gan(generator, discriminator)
	# load image data
	dataset = load_real_samples()
	# train model
	train(generator, discriminator, gan_model, dataset, latent_dim)

	# # FOR VIEWING
	# # load model
	# model = load_model('generator.h5')
	# # generate images
	# latent_points = generate_latent_points(100, 100)
	# # generate images
	# X = model.predict(latent_points)
	# # plot the result
	# show_plot(X, 10)
	breakpoint()
![](data/training.gif)

# Climb A-GAN

Climb A-GAN is a final project for Yale's CPSC 459, *Building Interactive Machines.*

## Repository Structure

This repo contains all code to work with our climbing GAN, but not the training data. Here's an overview of the directory structure:

* ``gan-test`` contains all our initial experimenting with GANs, ordered by the different explorations. The third directory contains the code which served as a base for Climb A-GAN.
* ``data`` contains a Python script to pull Moonboard problem data into this directory, some R code to explore the dataset, and an .html output of the R document to see the results.
* ``climbagan`` contains all the training and testing code related to Climb A-GAN. **Note these are all interactive python notebooks, meant to be run in Google Colaboratory!**
    * ``ACGAN-Moonboard``: where the magic really happens. Creates and trains GAN models.
    * ``Demo``: loads a generator model and displays examples one at a time.
    * ``MB Classifier``: explores possible model architectures for the discriminator.
    * ``ACGAN-Moonboard-MLP-Take2``: another take on hyperparameter searching with MLP models informed by the classifier exploration.

## Running the code

All GAN code for the Moonboard was written and run in Google Colaboratory to facilitate training. File I/O (reading training data, writing out models/etc.) occurs on Google Drive through Google File Stream. Thus our code expects the user to have a directory named ``colab/`` in the root directory of their Google Drive for all I/O. This directory should contain a ``data/`` folder with the problem data (e.g. ``data_clean.json``) inside. The name and location of this project can be changed in the main ACGAN noteobok, however.

### Training models:

``ACGAN-Moonboard`` and ``ACGAN-Moonboard-MLP-Take2`` can be run in Google Colaboratory, simply by executing each block at a time. Tensorboard data will be saved to ``logs/``, and model data (checkpoints and sample images) will be saved to a new directory according to the name of the model.

Many details can be configured by modifying the ``DEFAULT_CONFIG`` dictionary defined in the first code block.

### Demo a generator:

The ``Demo`` notebook expects a generator model to be saved in the data folder of project's root directory (e.g. ``/content/drive/My Drive/colab/data``). Running the script will give a basic, interactive interface for the user to input a climbing grade (e.g. 7A) and view a generated output.

## Contributions

* **Cove Geary** - *Initial work*
* **Joseph Valdez** - *Initial work*

Everything in the project was shared and attempted in a joint effort. The coding part of the project was weighted more towards Cove, as he tried many of the different things with the genertor. The writing and research part of the project was weighted more towards Joseph. 

## License

## Acknowledgments

* A
* B
* C
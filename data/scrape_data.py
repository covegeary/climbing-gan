from pathlib import Path
import requests
import simplejson as json

PAGES = 317  # 316 + 1
PAGE_LEN = 99
V_COOKIE = ''  # Fill these in with your own cookie data
M_COOKIE = ''  # Fill these in with your own cookie data
WANTED_KEYS = ['Method', 'Name', 'Grade', 'UserGrade', 'Rating', 'UserRating', 'Repeats', 'Attempts', 'Holds']

# Header for the POST request. Constant.
header = {
    'Accept': '*/*',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'X-Requested-With': 'XMLHttpRequest',
    'Origin': 'https://moonboard.com',
    'Referer': 'https://moonboard.com/Problems/Index',
    'Cookie': '__RequestVerificationToken={}; _MoonBoard={}'.format(V_COOKIE, M_COOKIE)
}


# Performs the HTTP(s) post. If success, returns problems as list of dicts
def post(page, n):
    print('POSTing for p{} (n={})'.format(page, n))
    data = { 'sort': '', 'page': str(page), 'pageSize': str(n), 'group': '', 'filter': "setupId~eq~'1'" }  # PSA. setupId determines whether we filter for 2016 problems or other setups
    r = requests.post('https://moonboard.com/Problems/GetProblems', headers=header, data=data)
    response_data = r.json()['Data']
    if not response_data or r.status_code != 200:
        print('Failed reponse on p{}. Status: {}'.format(page, r.status_code))
        return None
    return response_data


def clean_problem(prob):
    # Clean up the holds list: so each move is just a 3-tuple (str coordinate, bool is_start, bool is_end)
    prob['Holds'] = {'Start': [], 'End': [], 'Others': []}
    for m in prob['Moves']:
        if m['IsStart']: prob['Holds']['Start'].append(m['Description'])
        elif m['IsEnd']: prob['Holds']['End'].append(m['Description'])
        else: prob['Holds']['Others'].append(m['Description'])

    # Filter by keys
    prob = {k: v for k, v in prob.items() if k in WANTED_KEYS}
    return prob


def write_all_probs(filename):
    # Check for existence of data file (don't overwrite)
    p = Path(filename)
    if p.exists():
        print('File {} already exists. Exiting'.format(filename))
        return
    
    # Download all probs, iterating page by page
    all_probs = []
    for i in range(PAGES):
        response = post(i, PAGE_LEN)
        # Bad response => we're done.
        if not response:   
            break
        # Else, iterate through page of probs and keep the data we want
        for problem in response:
            all_probs.append(clean_problem(problem))
    
    # Write em.
    print('Downloaded {} problems. Writing...'.format(len(all_probs)))
    with p.open('w') as f:
        json.dump(all_probs, f, separators=(',', ':'), iterable_as_array=True)  # Last arg lets us pass in the dict generator from clean_problem
    
    return all_probs  # A list of dicts


def search_pagelim():
    # MoonBoard currently reports 26917 probs for 2016. Should be 272 pages of <=99 probs, theoretically??
    for i in range(400, 200, -1):
        response = post(i, 99)
        if response:
            breakpoint()
            break
            # As of 11/9/19, we get a response through p.316


if __name__ == '__main__':
    # To find the upper limit on number of pages
    # search_pagelim()

    # To download data
    probs = write_all_probs('data.json')
    
    # all_grades = ['6A', '6A+', '6B', '6B+', '6C', '6C+',
    #               '7A', '7A+', '7B', '7B+', '7C', '7C+',
    #               '8A', '8A+', '8B', '8B+', '8C', '8C+']
    
    # problems_sorted = [[p for p in probs if p['Grade'].upper() == grade] for grade in all_grades]
    # breakpoint()
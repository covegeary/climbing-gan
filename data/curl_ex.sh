#!/bin/sh

# Here's an example of a cURL command to POST to the Moonboard website to get climbing route data.
# The response is in  JSON format, holding 15 climbs.

curl \
'https://moonboard.com/Problems/GetProblems' \
-H 'Accept: */*' \
-H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
-H 'X-Requested-With: XMLHttpRequest' \
-H 'Origin: https://moonboard.com' \
-H 'Referer: https://moonboard.com/Problems/Index' \
-H 'Cookie: __RequestVerificationToken=FILL THIS IN; _MoonBoard=FILL THIS IN2' \
--data $'sort=&page=2&pageSize=15&group=&filter=setupId~eq~\'1\''